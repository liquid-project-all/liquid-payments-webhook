package webhook

import (
	"context"
	"liquid-payments-webhook/cmd/payments-webhook/config"
	"time"

	"github.com/pkg/errors"
	"github.com/stripe/stripe-go"
)

type Webhook interface {
	CheckoutSessionExpired(context.Context, *stripe.CheckoutSession) error
}

var _ Webhook = (*WebhookService)(nil)

type WebhookService struct {
	config *config.Config
	db     *DB
}

func NewWebhookService(config *config.Config, db *DB) *WebhookService {
	return &WebhookService{
		config: config,
		db:     db,
	}
}

func (w *WebhookService) CheckoutSessionExpired(ctx context.Context, event *stripe.CheckoutSession) error {

	err := w.db.SetOrderExpiredByCheckoutID(ctx, event.ID)
	if err != nil {
		return errors.Wrap(err, "SetOrderExpiredByCheckoutID")
	}

	return nil
}

func (w *WebhookService) CheckoutSessionCompleted(ctx context.Context, event *stripe.CheckoutSession) error {

	userID, err := w.db.GetUserIDByStripeID(ctx, event.Customer.ID)
	if err != nil {
		return errors.Wrap(err, "GetUserIDByStripeID")
	}

	sqlTx, err := w.db.sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return errors.Wrap(err, "BeginTx")
	}
	defer sqlTx.Rollback()

	tx := &Transaction{sqlTx}

	order, err := tx.LockOrderByCheckoutID(ctx, event.ID)
	if err != nil {
		return errors.Wrap(err, "LockOrderByCheckoutID")
	}

	order.IsPaid = true
	order.PaymentIntentID = event.PaymentIntent.ID
	order.Status = OrderStatusPaid
	order.PayTime = time.Now()

	err = tx.UpdateOrder(ctx, order)
	if err != nil {
		return errors.Wrap(err, "UpdateOrder")
	}

	gamesToAdd, err := tx.GetGamesByOrderID(ctx, order.ID)
	if err != nil {
		return errors.Wrap(err, "GetGamesByOrderID")
	}

	for _, gameID := range gamesToAdd {
		err = tx.AddUserGameConnection(ctx, userID, int64(gameID))
		if err != nil {
			return errors.Wrap(err, "AddUserGameConnection")
		}
	}

	err = tx.sqlTx.Commit()
	if err != nil {
		return errors.Wrap(err, "Commit")
	}

	return nil
}
