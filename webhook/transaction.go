package webhook

import (
	"context"
	"database/sql"
)

type Transaction struct {
	sqlTx *sql.Tx
}

func (tx *Transaction) AddOrder() error {

	return nil
}

func (tx *Transaction) LockOrderByCheckoutID(ctx context.Context, checkoutID string) (*Order, error) {
	rawSql := `SELECT id, create_time, pay_time,
	checkout_session_id, is_paid, status, user_id, total_price
	FROM orders WHERE checkout_session_id=$1 FOR UPDATE`

	order := &Order{}
	err := tx.sqlTx.QueryRowContext(ctx, rawSql, checkoutID).Scan(
		&order.ID,
		&order.CreateTime,
		&order.PayTime,
		&order.CheckoutSessionID,
		&order.IsPaid,
		&order.Status,
		&order.UserID,
		&order.TotalPrice,
	)
	if err != nil {
		return nil, err
	}

	return order, nil
}

func (tx *Transaction) UpdateOrder(ctx context.Context, order *Order) error {
	rawSql := `UPDATE orders SET pay_time=$1, is_paid=$2,
	status=$3, total_price=$4, payment_intent_id=$5`

	_, err := tx.sqlTx.ExecContext(
		ctx,
		rawSql,
		order.PayTime,
		order.IsPaid,
		order.Status,
		order.TotalPrice,
		order.PaymentIntentID,
	)

	return err
}

func (tx *Transaction) AddUserGameConnection(ctx context.Context, userID, gameID int64) error {
	rawSql := `INSERT INTO user_library (user_id, game_id) VALUES($1,$2)`

	_, err := tx.sqlTx.ExecContext(ctx, rawSql, userID, gameID)

	return err
}

func (tx *Transaction) SetOrderStatusByID(ctx context.Context, status string, orderID int64) error {
	rawSql := `UPDATE orders SET status=$1 WHERE id=$2`

	_, err := tx.sqlTx.ExecContext(ctx, rawSql, status, orderID)

	return err
}

func (tx *Transaction) GetGamesByOrderID(ctx context.Context, orderID int64) ([]int64, error) {
	rawSql := `SELECT game_id FROM order_games_connection WHERE order_id=$1`

	rows, err := tx.sqlTx.QueryContext(ctx, rawSql, orderID)
	if err != nil {
		return nil, err
	}

	var resultGames []int64
	for rows.Next() {
		var gameID int64
		rows.Scan(&gameID)
		resultGames = append(resultGames, gameID)
	}
	return resultGames, nil
}
