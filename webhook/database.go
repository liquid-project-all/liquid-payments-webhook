package webhook

import (
	"context"
	"database/sql"
	"fmt"
	"liquid-payments-webhook/cmd/payments-webhook/config"
	postgres "liquid-payments-webhook/libs/postgresql"
	"time"
)

const (
	OrderStatusCreated  = "created"
	OrderStatusPaid     = "paid"
	OrderStatusExpired  = "expired"
	OrderStatusRefunded = "refunded"
)

type Database interface {
	GetGameIDByStripeID(context.Context, string) (int64, error)
	GetUserIDByStripeID(context.Context, string) (int64, error)
	SetOrderExpiredByCheckoutID(context.Context, string) error
}

type DB struct {
	sqlDB  *sql.DB
	config *config.Config
}

var _ Database = (*DB)(nil)

func NewDB(config *config.Config) (*DB, error) {
	sqlDB, err := postgres.Connect(config)
	if err != nil {
		return nil, err
	}

	return &DB{
		sqlDB:  sqlDB,
		config: config,
	}, nil
}

type Order struct {
	ID                int64
	CreateTime        time.Time
	PayTime           time.Time
	PaymentIntentID   string
	CheckoutSessionID string
	IsPaid            bool
	Status            string
	UserID            int64
	TotalPrice        float64
}

type Games struct {
	ID          int64
	Title       string
	Genre       string
	Developer   string
	Publisher   string
	ReleaseDate int64
	CoverURL    string
	StripeID    string
}

type OrdersGamesConnection struct {
	ID      int64
	OrderID int64
	GameID  int64
}

type Users struct {
	ID         int64
	Login      string
	Password   string
	Email      string
	DateJoined int64
	LastLogin  int64
	StripeID   int64
}

type UsersLibrary struct {
	ID     int64
	UserID int64
	GameID int64
}

func (db *DB) GetGameIDByStripeID(ctx context.Context, stripeID string) (int64, error) {
	rawSql := `SELECT id FROM games WHERE stripe_id=$1`
	var ID int64

	err := db.sqlDB.QueryRowContext(ctx, rawSql, stripeID).Scan(&ID)
	if err != nil {
		return 0, err
	}

	fmt.Println("ID gry:")
	fmt.Println(ID)
	return ID, nil
}

func (db *DB) GetUserIDByStripeID(ctx context.Context, stripeID string) (int64, error) {
	rawSql := `SELECT id FROM users WHERE stripe_id=$1`
	var ID int64

	err := db.sqlDB.QueryRowContext(ctx, rawSql, stripeID).Scan(&ID)
	if err != nil {
		return 0, err
	}

	return ID, nil
}

// It's not needed to do this UPDATE inside of a SQL transaction
// It will be the only query run during handling checkout_session.expired
func (db *DB) SetOrderExpiredByCheckoutID(ctx context.Context, checkoutID string) error {
	rawSql := `UPDATE orders SET status=$1 WHERE checkout_id=$2`

	_, err := db.sqlDB.ExecContext(ctx, rawSql, OrderStatusExpired, checkoutID)

	return err
}
