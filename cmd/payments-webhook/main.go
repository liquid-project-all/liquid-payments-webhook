package main

import (
	"context"
	"liquid-payments-webhook/api"
	"liquid-payments-webhook/cmd/payments-webhook/config"
	"liquid-payments-webhook/webhook"

	"github.com/newrelic/go-agent/v3/integrations/logcontext/nrlogrusplugin"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

func main() {
	config := config.Parse()

	logger := log.New()
	logger.SetFormatter(nrlogrusplugin.ContextFormatter{})

	app, err := newrelic.NewApplication(
		newrelic.ConfigAppName(config.NewRelicAppName),
		newrelic.ConfigLicense(config.NewRelicKey),
		newrelic.ConfigDistributedTracerEnabled(true),
	)

	if err != nil {
		log.Errorf("unable to create new relic app session: %v", err)
	}

	txn := app.StartTransaction("dev-liquid-webhook")
	ctx := newrelic.NewContext(context.Background(), txn)
	logger.WithContext(ctx).Info("Logger for dev-liquid-webhook transaction started")

	txn.NoticeError(newrelic.Error{
		Message: "test error",
		Class:   "IdentifierForError",
		Attributes: map[string]interface{}{
			"important_number": 97232,
			"relevant_string":  "zap",
		},
	})

	database, err := webhook.NewDB(config)
	if err != nil {
		log.Panic(err)
	}

	webhookService := webhook.NewWebhookService(config, database)

	mainAPI := api.NewAPI(config)
	webhookAPI := api.NewWebhookAPI(config, mainAPI, webhookService, logger)
	webhookAPI.MuxRouter()

	err = mainAPI.Run()
	if err != nil {
		log.Panic(err)
	}
}
