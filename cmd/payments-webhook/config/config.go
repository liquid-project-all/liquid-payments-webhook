package config

import (
	"log"
	"time"

	"github.com/caarlos0/env"
)

type Config struct {
	Port            string        `env:"PORT" envDefault:"8080"`
	RequestTimeout  time.Duration `env:"REQUEST_TIMEOUT" envDefault:"5s"`
	DatabaseURL     string        `env:"DATABASE_URL"`
	Name            string        `env:"NAME" envDefault:"dev-payments-webhook"`
	NewRelicAppName string        `env:"NEW_RELIC_APP_NAME" envDefault:"dev-payments-webhook"`
	NewRelicKey     string        `env:"NEW_RELIC_KEY"`

	StripeKey      string `env:"STRIPE_KEY"`
	EndpointSecret string `env:"ENDPOINT_SECRET"`
}

func Parse() *Config {
	cfg := Config{}
	if err := env.Parse(&cfg); err != nil {
		log.Panicf("unable to parse config: %v", err)
	}
	return &cfg
}
