module liquid-payments-webhook

go 1.14

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/fasthttp/router v1.4.8
	github.com/klauspost/compress v1.15.2 // indirect
	github.com/lib/pq v1.10.5
	github.com/newrelic/go-agent/v3 v3.15.2
	github.com/newrelic/go-agent/v3/integrations/logcontext/nrlogrusplugin v1.0.1
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.1 // indirect
	github.com/stripe/stripe-go v70.15.0+incompatible
	github.com/valyala/fasthttp v1.36.0
)
