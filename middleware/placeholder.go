package middleware

import "github.com/valyala/fasthttp"

func Placeholder(next func(ctx *fasthttp.RequestCtx)) func(ctx *fasthttp.RequestCtx) {
	fn := func(ctx *fasthttp.RequestCtx) {
		defer func() {
			if ctx == nil {
				ctx.Error("error", 400)
			}
		}()
		// middleware logic here

		next(ctx)
	}
	return fn
}
