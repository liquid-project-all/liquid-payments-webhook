# liquid-api-payments-webhook

## What is it
Webhook app for receiving events from payment provider for the "Liquid" project
Main goal of this app is to correctly react to incoming notifications by performing SQL queries

## run locally
To run locally use "make run-local" command. For now docker-compose does not include usage of fake DB, because DB integration is not yet implemented into the app itself.