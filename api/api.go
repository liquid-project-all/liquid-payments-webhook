package api

import (
	"errors"
	"liquid-payments-webhook/cmd/payments-webhook/config"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

type API struct {
	config *config.Config
	router *router.Router
	server *fasthttp.Server
}

func NewAPI(cfg *config.Config) *API {
	return &API{
		config: cfg,
		router: router.New(),
	}
}

func (api *API) Run() error {
	if api.server != nil {
		return errors.New("server already running")
	}

	server := &fasthttp.Server{
		Handler:            api.router.Handler,
		Name:               api.config.Name,
		ReadBufferSize:     64 * 1024,
		MaxRequestBodySize: 16 * 1024 * 1024,
		ReadTimeout:        api.config.RequestTimeout,
	}
	api.server = server
	// https://github.com/tsingson/fasthttp-guide

	return server.ListenAndServe(":" + api.config.Port)
}

/*
func (api *API) shutdown() error {
	if api.server == nil {
		return errors.New("server not active")
	}

	server = api.server
	api.server = nil
	server.DisableKeepAlive = true
	return server.Shutdown()
}
*/
