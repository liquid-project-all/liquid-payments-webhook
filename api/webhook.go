package api

import (
	"encoding/json"
	"liquid-payments-webhook/cmd/payments-webhook/config"
	"liquid-payments-webhook/webhook"
	"net/http"

	log "github.com/sirupsen/logrus"
	"github.com/stripe/stripe-go"
	stripeWebhook "github.com/stripe/stripe-go/webhook"
	"github.com/valyala/fasthttp"
)

const (
	CheckoutSessionCompleted = "checkout.session.completed"
	CheckoutSessionExpired   = "checkout.session.expired"
)

type WebhookAPI struct {
	config         *config.Config
	mainAPI        *API
	webhookService *webhook.WebhookService
	logger         *log.Logger
}

func NewWebhookAPI(
	config *config.Config,
	mainAPI *API,
	webhookService *webhook.WebhookService,
	logger *log.Logger,
) *WebhookAPI {
	return &WebhookAPI{
		config:         config,
		mainAPI:        mainAPI,
		webhookService: webhookService,
		logger:         logger,
	}
}

func (api *WebhookAPI) MuxRouter() {
	api.mainAPI.router.GET("/healthcheck", api.healthCheckHandler())
	api.mainAPI.router.POST("/webhook", api.WebhookHandler())
}

func (api *WebhookAPI) healthCheckHandler() func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		ctx.SetContentType("application/json")
		ctx.SetStatusCode(http.StatusOK)
		ctx.SetBody([]byte(`healthy`))
		return
	}
}

func (api *WebhookAPI) WebhookHandler() func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		event, err := stripeWebhook.ConstructEvent(ctx.Request.Body(), string(ctx.Request.Header.Peek("Stripe-Signature")),
			api.config.EndpointSecret)
		if err != nil {
			api.logger.WithContext(ctx).
				Errorf("error constructing event: %v", err)
			ctx.SetStatusCode(http.StatusInternalServerError)
			return
		}

		switch event.Type {
		case CheckoutSessionCompleted:
			checkoutEvent := &stripe.CheckoutSession{}
			err = json.Unmarshal(event.Data.Raw, checkoutEvent)
			if err != nil {
				api.logger.WithContext(ctx).
					Errorf("unmarshalling: %v", err)
				ctx.SetStatusCode(http.StatusInternalServerError)
				return
			}

			err = api.webhookService.CheckoutSessionCompleted(ctx, checkoutEvent)
		case CheckoutSessionExpired:
			checkoutEvent := &stripe.CheckoutSession{}
			err = json.Unmarshal(event.Data.Raw, checkoutEvent)
			if err != nil {
				api.logger.WithContext(ctx).
					Errorf("unmarshalling: %v", err)
				ctx.SetStatusCode(http.StatusInternalServerError)
				return
			}
			err = api.webhookService.CheckoutSessionExpired(ctx, checkoutEvent)
		default:
			api.logger.WithContext(ctx).
				Warningf("unrecognised Stripe notification: %s ", event.Type)
		}

		if err != nil {
			api.logger.WithContext(ctx).
				Errorf("failed processing Stripe notification: %v", err)

			ctx.SetStatusCode(http.StatusInternalServerError)
			return
		}

		ctx.SetStatusCode(http.StatusOK)
		return
	}
}
