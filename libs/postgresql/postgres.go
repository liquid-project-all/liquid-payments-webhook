package postgres

import (
	"database/sql"

	"liquid-payments-webhook/cmd/payments-webhook/config"

	_ "github.com/lib/pq"
)

func Connect(config *config.Config) (*sql.DB, error) {
	db, err := sql.Open("postgres", config.DatabaseURL)
	if err != nil {
		return nil, err
	}

	return db, nil
}
